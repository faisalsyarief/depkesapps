-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Agu 2019 pada 11.16
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `depkesapps`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_depkes`
--

CREATE TABLE `data_depkes` (
  `id_depkes` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `no_va` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `jml_bulan_ipkl_terbayar` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `value_nilai` int(11) NOT NULL,
  `value_nilai_blm_terbayar` int(11) NOT NULL,
  `jml_bulan_ipkl_blm_terbayar` int(11) NOT NULL,
  `nilai_jml_bulan_ipkl` int(11) NOT NULL,
  `thr` int(11) NOT NULL,
  `fogging` int(11) NOT NULL,
  `hbh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_depkes`
--

INSERT INTO `data_depkes` (`id_depkes`, `nama`, `no_va`, `alamat`, `jml_bulan_ipkl_terbayar`, `tahun`, `value_nilai`, `value_nilai_blm_terbayar`, `jml_bulan_ipkl_blm_terbayar`, `nilai_jml_bulan_ipkl`, `thr`, `fogging`, `hbh`) VALUES
(11, 'Nama Lengkap', '123456789', 'Jakarta', 4, 2019, 700000, 1400000, 8, 175000, 300000, 70000, 150000),
(12, 'Nama Lengkap', '123456790', 'Jakarta', 8, 2019, 1400000, 700000, 4, 175000, 300000, 70000, 150000),
(13, 'Nama Lengkap', '123456789', 'Jakarta', 4, 2019, 700000, 1400000, 8, 175000, 300000, 70000, 150000),
(14, 'Nama Lengkap', '123456790', 'Jakarta', 8, 2019, 1400000, 700000, 4, 175000, 300000, 70000, 150000),
(15, 'Nama Lengkap', '123456789', 'Jakarta', 4, 2019, 700000, 1400000, 8, 175000, 300000, 70000, 150000),
(16, 'Nama Lengkap', '123456790', 'Jakarta', 8, 2019, 1400000, 700000, 4, 175000, 300000, 70000, 150000),
(17, 'Nama Lengkap', '123456789', 'Jakarta', 4, 2019, 700000, 1400000, 8, 175000, 300000, 70000, 150000),
(18, 'Nama Lengkap', '123456790', 'Jakarta', 8, 2019, 1400000, 700000, 4, 175000, 300000, 70000, 150000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dka_data_login`
--

CREATE TABLE `dka_data_login` (
  `id_login` varchar(10) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `gender` enum('Laki-laki','Perempuan') NOT NULL,
  `address` varchar(255) NOT NULL,
  `group_user` tinyint(1) NOT NULL,
  `signup_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dka_data_login`
--

INSERT INTO `dka_data_login` (`id_login`, `fullname`, `email`, `telp`, `username`, `password`, `gender`, `address`, `group_user`, `signup_date`) VALUES
('UDKA0001', 'Biro Sistem Informasi', 'bsi@gmail.com', '08123456789', 'admin', 'admin', 'Laki-laki', 'Jl. DI. Panjaitan, RT.1/RW.11, Cipinang Cempedak, Kecamatan Jatinegara, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13340', 1, '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_depkes`
--
ALTER TABLE `data_depkes`
  ADD PRIMARY KEY (`id_depkes`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_depkes`
--
ALTER TABLE `data_depkes`
  MODIFY `id_depkes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
