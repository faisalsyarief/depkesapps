<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Depkes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_Model_dka');
	}

	public function index()
	{
		$this->form_validation->set_rules('username','Username','required|alpha_numeric');
		$this->form_validation->set_rules('password','Password','required|alpha_numeric');
		
		if($this->form_validation->run() == FALSE){
			$this->load->view('/content/form_login_dka');
		}else{
			$this->load->model('User_Model_dka');
			$valid_user = $this->User_Model_dka->check_credential();
			
			if($valid_user == FALSE){
				$this->session->set_flashdata('error','Maaf, username atau password yang Anda masukkan salah. Silakan coba lagi');
				redirect('Login_Depkes');
			}else{
				$this->session->set_userdata('id_login',$valid_user->id_login);
				$this->session->set_userdata('username',$valid_user->username);
				$this->session->set_userdata('fullname',$valid_user->fullname);
				$this->session->set_userdata('group_user',$valid_user->group_user);
				
				switch($valid_user->group_user){
					case 1 :
					redirect('Admin_Depkes/Home_Depkes'); 
					break;
					default: 
					break;
				}
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());	
	}
	
	public function profile()
	{
		$data['user_data'] = $this->User_Model_dka->get_user_dka();
        $data['title'] = 'Profile';
		$this->load->view('template/header_dka', $data);
		$this->load->view('content/admin/master_user', $data);
		$this->load->view('template/footer_dka');
	}
	
	
	public function edit_user_depkes($id)
	{
		$this->form_validation->set_rules('fullname','Nama Lengkap','required');
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('telp','No Telepon','required|numeric');
		$this->form_validation->set_rules('username','Username','required|alpha_numeric|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('password','Password','required|alpha_numeric|min_length[8]|max_length[24]');
		$this->form_validation->set_rules('gender','Jenis Kelamin','required');
		$this->form_validation->set_rules('address','Alamat','required');
		
		if($this->form_validation->run() == FALSE){
			$data['user_depkes'] = $this->User_Model_dka->find($id);
			$data['title'] = 'Edit User Depkes';
			$this->load->view('template/header_dka', $data);
			$this->load->view('content/admin/edit_user', $data);
			$this->load->view('template/footer_dka');
		}else{
			$user_data = array (
				'fullname' => set_value('fullname'),
				'email' => set_value('email'),
				'telp' => set_value('telp'),
				'username' => set_value('username'),
				'password' => set_value('password'),
				'gender' => set_value('gender'),
				'address' => set_value('address'),
				'group_user' => '1'
			);
			$this->User_Model_dka->update_dka($id,$user_data);
			redirect('Login_Depkes/profile');
		}	
	}
}