<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_Depkes extends CI_Controller {
	private $filename = "import_data";

	public function __construct()
	{
		parent::__construct();
		
		if($this->session->userdata('group_user') != '1'){
			$this->session->set_flashdata('error','Maaf, silahkan login admin terlebih dahulu!');
			redirect('Login_Depkes');
		}
		$this->load->model('Master_Model_dka');
	}

	public function index()
	{
        $data['title'] = 'Dashboard';
		$this->load->view('template/header_dka', $data);
		$this->load->view('content/admin/dasboard_dka', $data);
		$this->load->view('template/footer_dka');
	}
	
	public function view_master_depkes()
	{
		$data['master_depkes'] = $this->Master_Model_dka->get_master_dka();
        $data['title'] = 'Master Depkes';
		$this->load->view('template/header_dka', $data);
		$this->load->view('content/master_depkes/master_depkes', $data);
		$this->load->view('template/footer_dka');
	}

	public function preview_master_dekpes($id)
	{
		$data['master_depkes'] = $this->Master_Model_dka->detail($id);
        $data['title'] = 'Preview Depkes';
		$this->load->view('template/header_dka', $data);
		$this->load->view('content/master_depkes/preview_dka', $data);
		$this->load->view('template/footer_dka');
	}

	public function print_pdf_perid($id)
	{
		$data['master_depkes'] = $this->Master_Model_dka->detail($id);
        $data['title'] = 'Preview Depkes';
		$this->load->view('template/header_dka', $data);
		$this->load->view('content/master_depkes/print_pdf_per_id', $data);
		$this->load->view('template/footer_dka');
	}

	public function delete_master_depkes($id)
	{
		$this->Master_Model_dka->get_delete_master_dka($id);
		redirect('Admin_Depkes/Home_Depkes/view_master_depkes');	
	}
	
	public function add_master_depkes()
	{
		$this->form_validation->set_rules('nama','Nama Lengkap','required');
		$this->form_validation->set_rules('no_va','No Virtual Akun','required|numeric|is_unique[data_depkes.no_va]');
		$this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('jml_bulan_ipkl_terbayar','Jumlah Bulan IPKL Terbayar','required|numeric|max_length[2]');
		$this->form_validation->set_rules('jml_bulan_ipkl_blm_terbayar','Jumlah Bulan IPKL Belum Terbayar','required|numeric|max_length[2]');
		$this->form_validation->set_rules('tahun','Tahun','required|numeric|max_length[4]');
		$this->form_validation->set_rules('nilai_jml_bulan_ipkl','Nilai IPKL','required|numeric');
		$this->form_validation->set_rules('thr','THR','required|numeric');
		$this->form_validation->set_rules('fogging','FOGGING','required|numeric');
		$this->form_validation->set_rules('hbh','HBH','required|numeric');
		
		if($this->form_validation->run() == FALSE){
			$data['title'] = 'Tambah Master Depkes';
			$this->load->view('template/header_dka', $data);
			$this->load->view('content/master_depkes/add_master_dka', $data);
			$this->load->view('template/footer_dka');
		}else{
			$master_data = array (
				'nama' => set_value('nama'),
				'no_va' => set_value('no_va'),
				'alamat' => set_value('alamat'),
				'jml_bulan_ipkl_terbayar' => set_value('jml_bulan_ipkl_terbayar'),
				'tahun' => set_value('tahun'),
				'value_nilai' => set_value('value_nilai'),
				'value_nilai_blm_terbayar' => set_value('value_nilai_blm_terbayar'),
				'jml_bulan_ipkl_blm_terbayar' => set_value('jml_bulan_ipkl_blm_terbayar'),
				'nilai_jml_bulan_ipkl' => set_value('nilai_jml_bulan_ipkl'),
				'thr' => set_value('thr'),
				'fogging' => set_value('fogging'),
				'hbh' => set_value('hbh')
			);
			$this->Master_Model_dka->insert_dka($master_data);
			redirect('Admin_Depkes/Home_Depkes/view_master_depkes');
		}	
	}
	
	public function edit_master_depkes($id)
	{
		$this->form_validation->set_rules('nama','Nama Lengkap','required');
		$this->form_validation->set_rules('no_va','No Virtual Akun','required|numeric');
		$this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('jml_bulan_ipkl_terbayar','Jumlah Bulan IPKL Terbayar','required|numeric|max_length[2]');
		$this->form_validation->set_rules('jml_bulan_ipkl_blm_terbayar','Jumlah Bulan IPKL Belum Terbayar','required|numeric|max_length[2]');
		$this->form_validation->set_rules('tahun','Tahun','required|numeric|max_length[4]');
		$this->form_validation->set_rules('nilai_jml_bulan_ipkl','Nilai IPKL','required|numeric');
		$this->form_validation->set_rules('thr','THR','required|numeric');
		$this->form_validation->set_rules('fogging','FOGGING','required|numeric');
		$this->form_validation->set_rules('hbh','HBH','required|numeric');
		
		if($this->form_validation->run() == FALSE){
			
			$data['master_depkes'] = $this->Master_Model_dka->find($id);
			$data['title'] = 'Edit Master Depkes';
			$this->load->view('template/header_dka', $data);
			$this->load->view('content/master_depkes/edit_master_dka', $data);
			$this->load->view('template/footer_dka');
		}else{
			$master_data = array (
				'nama' => set_value('nama'),
				'no_va' => set_value('no_va'),
				'alamat' => set_value('alamat'),
				'jml_bulan_ipkl_terbayar' => set_value('jml_bulan_ipkl_terbayar'),
				'tahun' => set_value('tahun'),
				'value_nilai' => set_value('value_nilai'),
				'value_nilai_blm_terbayar' => set_value('value_nilai_blm_terbayar'),
				'jml_bulan_ipkl_blm_terbayar' => set_value('jml_bulan_ipkl_blm_terbayar'),
				'nilai_jml_bulan_ipkl' => set_value('nilai_jml_bulan_ipkl'),
				'thr' => set_value('thr'),
				'fogging' => set_value('fogging'),
				'hbh' => set_value('hbh')
			);
			$this->Master_Model_dka->update_dka($id,$master_data);
			redirect('Admin_Depkes/Home_Depkes/view_master_depkes');
		}	
	}

	//Import Excel
	public function form()
	{
		$data = array();
		if(isset($_POST['preview'])){
			$upload = $this->Master_Model_dka->upload_file($this->filename);
			if($upload['result'] == "success"){
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';
				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('assets/Excel/'.$this->filename.'.xlsx');
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
				$data['sheet'] = $sheet; 
			}else{
				$data['upload_error'] = $upload['error'];
			}
		}
        $data['title'] = 'Import Excel';
		$this->load->view('template/header_dka', $data);
		$this->load->view('content/master_depkes/form', $data);
		$this->load->view('template/footer_dka');
	}
		
	public function import()
	{
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('assets/Excel/'.$this->filename.'.xlsx');
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		$data = [];
		$numrow = 1;
		foreach($sheet as $row){
			if($numrow > 1){
				array_push($data, [
					'nama' => $row['A'],
					'no_va' => $row['B'],
					'alamat' => $row['C'],
					'jml_bulan_ipkl_terbayar' => $row['D'],
					'value_nilai' => $row['E'],
					'jml_bulan_ipkl_blm_terbayar' => $row['F'],
					'value_nilai_blm_terbayar' => $row['G'],
					'tahun' => $row['H'],
					'nilai_jml_bulan_ipkl' => $row['I'],
					'thr' => $row['J'],
					'fogging' => $row['K'],
					'hbh' => $row['L'],
					]);
				}
				$numrow++;
			}
			$this->Master_Model_dka->insert_multiple($data);
			redirect("Admin_Depkes/Home_Depkes/view_master_depkes"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}
	//Import Excel

	public function print_pdf($id)
	{
		$data = [];
		$data['master_depkes'] = $this->Master_Model_dka->detail($id);
		$html=$this->load->view('content/master_depkes/print_pdf_per_id', $data, true);
		$pdfFilePath = "output_pdf_name.pdf";
		$this->load->library('m_pdf');
		$this->m_pdf->pdf->WriteHTML($html);
		$this->m_pdf->pdf->Output($pdfFilePath, "D");		
	}

	public function print_pdf_all()
	{
		$data = [];
		$data['master_depkes'] = $this->Master_Model_dka->get_master_dka();
		$html=$this->load->view('content/master_depkes/print_pdf_per_all', $data, true);
		$pdfFilePath = "output_pdf_name.pdf";
		$this->load->library('m_pdf');
		$this->m_pdf->pdf->WriteHTML($html);
		$this->m_pdf->pdf->Output($pdfFilePath, "D");		
	}

}
