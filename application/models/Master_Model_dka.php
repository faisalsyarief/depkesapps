<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Model_dka extends CI_Model {
	    
	public function get_master_dka()
	{
		$hasil=$this->db->get('data_depkes');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		}else{
			return false;
		}
	}

	public function get_delete_master_dka($id)
	{
		$this->db->where('id_depkes',$id)
		->delete('data_depkes');
	}

	public function insert_dka($master_data)
	{
		$this->db->insert('data_depkes',$master_data);
	}

	public function update_dka($id, $master_data)
	{
		$this->db->where('id_depkes',$id)
		->update('data_depkes',$master_data);	
	}

	public function find($id)
	{
		$hasil = $this->db->where('id_depkes',$id)
		->limit(1)
		->get('data_depkes');
		if($hasil->num_rows() > 0){
			return $hasil->row();
		}else{
			return array();
		}
	}
	
	public function detail($id)
	{
		$hasil = $this->db->where('id_depkes',$id)
		->limit(1)
		->get('data_depkes');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		}else{
			return array();
		}
	}

	//Import Excel
	public function upload_file($filename)
	{
		$this->load->library('upload');
		$config['upload_path'] = './assets/excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size'] = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;
		$this->upload->initialize($config);
		if($this->upload->do_upload('file')){
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}
	
	public function insert_multiple($data)
	{
		$this->db->insert_batch('data_depkes', $data);
	}
	//Import Excel

}