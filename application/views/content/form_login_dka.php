<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Depkes Login Form</title>

  <link href="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">

  <link href="<?php echo base_url('assets/Template_Bootstrap_Free/css/sb-admin.css'); ?>" rel="stylesheet">
</head>

<body class="bg-dark">

  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
      <?php
      $message = $this->session->flashdata('error');
      if($message){
        echo '<div class="text-center alert alert-danger">' .$message. '</div>';
      } ?>
        <?=form_open('Login_Depkes',['class'=>'form-horizontal'])?>
          <?php $error = form_error("username", "<p class='text-danger'>", '</p>'); ?>
          <div class="form-group">
            <div class="form-label-group <?php echo $error ? 'has-error' : '' ?>">
              <input type="text" id="username" name="username" class="form-control" placeholder="Username" required="required" autofocus="autofocus">
              <label for="username">Username</label>
            </div>
          </div>
          <?php echo $error; ?>

          <?php $error = form_error("password", "<p class='text-danger'>", '</p>'); ?>
          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
            <div class="form-label-group">
              <input type="password" id="password" name="password" class="form-control" required="required">
              <label for="inputPassword">Password</label>
            </div>
          </div>
          <?php echo $error; ?>

          <button type="submit" class="btn btn-primary btn-block">Login</button>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="<?php echo base_url(); ?>">Depkes Apps Login</a>
        </div>
      </div>
    </div>
  </div>

  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

</body>
</html>