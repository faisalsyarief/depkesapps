<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        <?=$title?> 
        <font color='#e9ecef'> || </font><a href='<?php echo base_url("Admin_Depkes/Home_Depkes"); ?>' class='btn btn-success btn-sm'><i class="fa fa-backward" aria-hidden="true"> Kembali </i></a></div>
    <div class="card-body">
          
        <div class="table-responsive">
          <?php foreach($user_data as $data) : ?>
          	<table class="table table-hover">
                <tr>
                  <td><strong>Nama Lengkap</strong></td>
                  <td>:</td>
                  <td><?=$data->fullname?></td>
                </tr>
                <tr>
                  <td><strong>Username</strong></td>
                  <td>:</td>
                  <td><?=$data->username?></td>
                </tr>
                <tr>
                  <td><strong>Password</strong></td>
                  <td>:</td>
                  <td><?=$data->password?></td>
                </tr>
                <tr>
                  <td><strong>Jenis Kelamin</strong></td>
                  <td>:</td>
                  <td><?=$data->gender?></td>
                </tr>
                <tr>
                  <td><strong>Alamat</strong></td>
                  <td>:</td>
                  <td><?=$data->address?></td>
                </tr>
                <tr>
                  <td><strong>Email</strong></td>
                  <td>:</td>
                  <td><?=$data->email?></td>
                </tr>
                <tr>
                  <td><strong>Telp</strong></td>
                  <td>:</td>
                  <td><?=$data->telp?></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td align="right"><?=anchor('Login_Depkes/edit_user_depkes/' . $data->id_login,'<i class="fa fa-pencil-square-o" aria-hidden="true"> Ubah</i>',['class'=>'btn btn-warning btn-sm'])?></td>
                </tr>
            </table><br>
            <?php endforeach; ?>
        </div>

        </div>
    </div>