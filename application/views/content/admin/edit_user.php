<?php
	$id_login = $user_depkes->id_login;
if($this->input->post('is_submitted')){
	$fullname = set_value('fullname');
	$email = set_value('email');
	$telp = set_value('telp');
	$username = set_value('username');
	$password = set_value('password');
	$gender = set_value('gender');
	$address = set_value('address');
}else{
	$fullname = $user_depkes->fullname;
	$email = $user_depkes->email;
	$telp = $user_depkes->telp;
	$username = $user_depkes->username;
	$password = $user_depkes->password;
	$gender = $user_depkes->gender;
	$address = $user_depkes->address;
}
?>
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        <?=$title?></div>
    <div class="card-body">
        
    <ol class="breadcrumb">
        <a href='<?php echo base_url("Login_Depkes/profile"); ?>' class='btn btn-success btn-sm'><i class="fa fa-backward" aria-hidden="true"> Kembali </i></a>
        <div style="clear: both;"></div>
    </ol>

        <div class="col-md-12">
	       <?=form_open_multipart('Login_Depkes/edit_user_depkes/' . $id_login,['class'=>'form-horizontal'])?>
	       <?php $error = form_error("fullname", "<p class='text-danger'>", '</p>'); ?>
	         <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	         	<label class="col-sm-2 control-label">Nama Lengkap :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="fullname" value="<?= $fullname ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("email", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Email :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="email" value="<?= $email ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>

	          <?php $error = form_error("telp", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Telp :</label>
	            <div class="col-sm-10">
	              <textarea class="form-control" class="form-control" name="telp"><?= $telp ?></textarea>
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("username", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Username :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="username" value="<?= $username ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("password", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Password :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="password" value="<?= $password ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <div class="form-group">
	          	<label class="col-sm-2 control-label">Jenis Kelamin</label>
	            <div class="col-sm-10">
	            <?php if($gender == 'Laki-laki'){
	            	echo ('
	            		<input type="radio" name="gender" value="Laki-laki" checked>
	                	Laki-laki<br>
	            
	                	<input type="radio" name="gender" value="Perempuan">
	                	Perempuan');
                }else{
                    echo ('
                    	<input type="radio" name="gender" value="Laki-laki">
	                	Laki-laki<br>
	            
	                	<input type="radio" name="gender" value="Perempuan" checked>
	                	Perempuan');
                } ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("address", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Alamat :</label>
	            <div class="col-sm-10">
	              <textarea type="text" class="form-control" name="address"><?= $address ?></textarea>
	              <?php echo $error; ?>
	            </div>
	          </div>
	          	          
	          <div class="form-group">
	            <div class="col-sm-offset-2 col-sm-10">
	              <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-check"> Simpan</i></button>
	            </div>
	          </div>
	       </form>
        </div>

        </div>
    </div>