<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        <?=$title?> 
        <font color='#e9ecef'> || </font><a href='<?php echo base_url("Admin_Depkes/Home_Depkes/view_master_depkes"); ?>' class='btn btn-success btn-sm'><i class="fa fa-backward" aria-hidden="true"> Kembali </i></a></div>
    <div class="card-body">
          
        <div class="table-responsive">
          <?php foreach($master_depkes as $data) : ?>
          	<table class="table table-hover">
                <tr>
                  <td><strong>Nama Lengkap</strong></td>
                  <td>:</td>
                  <td><?=$data->nama?></td>
                </tr>
                <tr>
                  <td><strong>No VA</strong></td>
                  <td>:</td>
                  <td><?=$data->no_va?></td>
                </tr>
                <tr>
                  <td><strong>Alamat</strong></td>
                  <td>:</td>
                  <td><?=$data->alamat?></td>
                </tr>
            </table><br>
            <table class="table table-hover" id="table-data" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>JENIS IURAN</th>
                    <th>NILAI</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>JML BULAN IPKL 2019 TERBAYAR</td>
                    <td><?="Rp " . number_format($data->value_nilai,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td>2</td>
                    <td>JML BULAN IPKL 2019 BELUM TERBAYAR</td>
                    <td><?="Rp " . number_format($data->value_nilai_blm_terbayar,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td>3</td>
                    <td>THR</td>
                    <td><?="Rp " . number_format($data->thr,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td>4</td>
                    <td>FOGGING</td>
                    <td><?="Rp " . number_format($data->fogging,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td>5</td>
                    <td>HBH</td>
                    <td><?="Rp " . number_format($data->hbh,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td colspan='2'>JUMLAH</td>
                    <td><?="Rp " . number_format($hasil=$data->value_nilai + $data->value_nilai_blm_terbayar + $data->thr + $data->fogging + $data->hbh,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td colspan='3'>
                        <?=anchor('Admin_Depkes/Home_depkes/print_pdf/' . $data->id_depkes,'<i class="fa fa-print" aria-hidden="true"> Print</i>',['class'=>'btn btn-primary btn-sm'])?>
                    </td>
                  <tr>
                </tbody>
            </table><br>
            <?php endforeach; ?>
        </div>

        </div>
    </div>