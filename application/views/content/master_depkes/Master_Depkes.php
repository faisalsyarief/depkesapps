        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            <?=$title?></div>
          <div class="card-body">
              
            <ol class="breadcrumb">
              <?=anchor('Admin_Depkes/Home_Depkes/add_master_depkes','<font color="black"><i class="fa fa-plus-square-o" aria-hidden="true"> Tambah Data</i></font>',['class'=>'btn btn-success btn-sm','style'=>'float:Left;'])?>
              <div style="clear: both;"></div>
            </ol>
            
            <a href="<?php echo base_url("Admin_Depkes/Home_Depkes/form"); ?>" class="btn btn-primary btn-sm"><i class="fa fa-clone" aria-hidden="true"> Import Data</i></a>
            <font color='white'> || </font><a href="<?php echo base_url("Admin_Depkes/Home_depkes/print_pdf_all"); ?>" class="btn btn-primary btn-sm"><i class="fa fa-print" aria-hidden="true"> Print Semua Data</i></a><br><br>
            
            <div class="table-responsive">
              <table class="table table-striped table-bordered dt-responsive nowrap" id="table-data" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NO VA</th>
                    <th>Alamat</th>
                    <th>Jumlah Bulan IPKL Terbayar</th>
                    <th>Nilai Bulan IPKL Terbayar</th>
                    <th>Jumlah Bulan IPKL Belum Terbayar</th>
                    <th>Nilai Bulan IPKL Belum Terbayar</th>
                    <th>Tahun</th>
                    <th>Nilai</th>
                    <th>THR</th>
                    <th>Fogging</th>
                    <th>HBH</th>
                    <th>Fungsi</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NO VA</th>
                    <th>Alamat</th>
                    <th>Jumlah Bulan IPKL Terbayar</th>
                    <th>Nilai Bulan IPKL Terbayar</th>
                    <th>Jumlah Bulan IPKL Belum Terbayar</th>
                    <th>Nilai Bulan IPKL Belum Terbayar</th>
                    <th>Tahun</th>
                    <th>Nilai</th>
                    <th>THR</th>
                    <th>Fogging</th>
                    <th>HBH</th>
                    <th>Fungsi</th>
                  </tr>
                </tfoot>
                <tbody>
                <?php if($master_depkes){ ?>
                  <?php 
                  $i=1;
                  foreach($master_depkes as $data) : ;?>
                  <tr>
                    <td><?=$i?></td>
                    <td><?=$data->nama?></td>
                    <td><?=$data->no_va?></td>
                    <td><?=$data->alamat?></td>
                    <td><?=$data->jml_bulan_ipkl_terbayar?></td>
                    <td><?="Rp " . number_format($data->value_nilai,2,',','.');?></td>
                    <td><?=$data->jml_bulan_ipkl_blm_terbayar?></td>
                    <td><?="Rp " . number_format($data->value_nilai_blm_terbayar,2,',','.');?></td>
                    <td><?=$data->tahun?></td>
                    <td><?="Rp " . number_format($data->nilai_jml_bulan_ipkl,2,',','.');?></td>
                    <td><?="Rp " . number_format($data->thr,2,',','.');?></td>
                    <td><?="Rp " . number_format($data->fogging,2,',','.');?></td>
                    <td><?="Rp " . number_format($data->hbh,2,',','.');?></td>
                    <td>
                        <?=anchor('Admin_Depkes/Home_depkes/preview_master_dekpes/' . $data->id_depkes,'<i class="fa fa-print" aria-hidden="true"> Preview</i>',['class'=>'btn btn-primary btn-sm'])?>
                        <font color='white'> || </font><?=anchor('Admin_Depkes/Home_depkes/edit_master_depkes/' . $data->id_depkes,'<i class="fa fa-pencil-square-o" aria-hidden="true"> Ubah</i>',['class'=>'btn btn-warning btn-sm'])?>
                        <font color='white'> || </font><?=anchor('Admin_Depkes/Home_depkes/delete_master_depkes/' . $data->id_depkes,'<i class="fa fa-trash-o" aria-hidden="true"> Hapus</i>',['class'=>'btn btn-danger btn-sm','onclick'=>'return confirm(\'Apakah anda yakin?\')'])?>
                    </td>
                  </tr>
                  <?php 
                  $i++;
                  endforeach; ?>
                <?php }else{ ?>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>