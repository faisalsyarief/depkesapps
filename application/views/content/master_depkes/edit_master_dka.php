<?php
	$id_depkes = $master_depkes->id_depkes;
if($this->input->post('is_submitted')){
	$nama = set_value('nama');
	$no_va = set_value('no_va');
	$alamat = set_value('alamat');
	$jml_bulan_ipkl_terbayar = set_value('jml_bulan_ipkl_terbayar');
	$jml_bulan_ipkl_blm_terbayar = set_value('jml_bulan_ipkl_blm_terbayar');
	$tahun = set_value('tahun');
	$value_nilai = set_value('value_nilai');
	$value_nilai_blm_terbayar = set_value('value_nilai_blm_terbayar');
	$nilai_jml_bulan_ipkl = set_value('nilai_jml_bulan_ipkl');
	$thr = set_value('thr');
	$fogging = set_value('fogging');
	$hbh = set_value('hbh');
}else{
	$nama = $master_depkes->nama;
	$no_va = $master_depkes->no_va;
	$alamat = $master_depkes->alamat;
	$jml_bulan_ipkl_terbayar = $master_depkes->jml_bulan_ipkl_terbayar;
	$jml_bulan_ipkl_blm_terbayar = $master_depkes->jml_bulan_ipkl_blm_terbayar;
	$tahun = $master_depkes->tahun;
	$value_nilai = $master_depkes->value_nilai;
	$value_nilai_blm_terbayar = $master_depkes->value_nilai_blm_terbayar;
	$nilai_jml_bulan_ipkl = $master_depkes->nilai_jml_bulan_ipkl;
	$thr = $master_depkes->thr;
	$fogging = $master_depkes->fogging;
	$hbh = $master_depkes->hbh;
}
?>
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        <?=$title?></div>
    <div class="card-body">
        
    <ol class="breadcrumb">
        <a href='<?php echo base_url("Admin_Depkes/Home_Depkes/view_master_depkes"); ?>' class='btn btn-success btn-sm'><i class="fa fa-backward" aria-hidden="true"> Kembali </i></a>
        <div style="clear: both;"></div>
    </ol>

        <div class="col-md-12">
	       <?=form_open_multipart('Admin_Depkes/Home_Depkes/edit_master_depkes/' . $id_depkes,['class'=>'form-horizontal'])?>
	       <?php $error = form_error("nama", "<p class='text-danger'>", '</p>'); ?>
	         <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	         	<label class="col-sm-2 control-label">Nama Lengkap :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="nama" value="<?= $nama ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("no_va", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">No VA :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="no_va" value="<?= $no_va ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>

	          <?php $error = form_error("alamat", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Alamat :</label>
	            <div class="col-sm-10">
	              <textarea class="form-control" class="form-control" name="alamat"><?= $alamat ?></textarea>
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("jml_bulan_ipkl_terbayar", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Jumlah Bulan IPKL Terbayar:</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" onchange="hitung();" id="jml_bulan_ipkl_terbayar" name="jml_bulan_ipkl_terbayar" value="<?= $jml_bulan_ipkl_terbayar ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("jml_bulan_ipkl_blm_terbayar", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Jumlah Bulan IPKL Belum Terbayar:</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" onchange="hitung();" id="jml_bulan_ipkl_blm_terbayar" name="jml_bulan_ipkl_blm_terbayar" value="<?= $jml_bulan_ipkl_blm_terbayar ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("tahun", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Tahun</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="tahun" value="<?= $tahun ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("nilai_jml_bulan_ipkl", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Nilai IPKL :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" onchange="hitung();" id="nilai_jml_bulan_ipkl" name="nilai_jml_bulan_ipkl" value="<?= $nilai_jml_bulan_ipkl ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("value_nilai", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Nilai Bulan IPKL Terbayar:</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" id="value_nilai" name="value_nilai" value="<?= $value_nilai ?>" readonly>
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("value_nilai_blm_terbayar", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Nilai Bulan IPKL Belum Terbayar:</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" id="value_nilai_blm_terbayar" name="value_nilai_blm_terbayar" value="<?= $value_nilai_blm_terbayar ?>" readonly>
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("thr", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">THR :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="thr" value="<?= $thr ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("fogging", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">FOGGING :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="fogging" value="<?= $fogging ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("hbh", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">HBH :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="hbh" value="<?= $hbh ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          	          
	          <div class="form-group">
	            <div class="col-sm-offset-2 col-sm-10">
	              <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-check"> Simpan</i></button>
	            </div>
	          </div>
	       </form>
        </div>

        </div>
    </div>
    
    <script>
        function hitung() {
            var number1=document.getElementById("jml_bulan_ipkl_terbayar").value;
            var number2=document.getElementById("jml_bulan_ipkl_blm_terbayar").value;
            var number3=document.getElementById("nilai_jml_bulan_ipkl").value;
            hasil1=parseInt(number1)*parseInt(number3);
            hasil2=parseInt(number2)*parseInt(number3);
            document.getElementById("value_nilai").value=hasil1;
            document.getElementById("value_nilai_blm_terbayar").value=hasil2;
        }
    </script>