<div class="table-responsive">
          <?php foreach($master_depkes as $data) : ?>
          	<table class="table table-hover">
                <tr>
                  <td><strong>Nama Lengkap</strong></td>
                  <td>:</td>
                  <td><?=$data->nama?></td>
                </tr>
                <tr>
                  <td><strong>No VA</strong></td>
                  <td>:</td>
                  <td><?=$data->no_va?></td>
                </tr>
                <tr>
                  <td><strong>Alamat</strong></td>
                  <td>:</td>
                  <td><?=$data->alamat?></td>
                </tr>
            </table><br>
            <table class="table table-hover" id="table-data" width="100%" cellspacing="0" border="1">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>JENIS IURAN</th>
                    <th>NILAI</th>
                  </tr>
                </thead>
                <tbody border='1'>
                  <tr>
                    <td>1</td>
                    <td>JML BULAN IPKL 2019 TERBAYAR</td>
                    <td><?="Rp " . number_format($data->value_nilai,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td>2</td>
                    <td>JML BULAN IPKL 2019 BELUM TERBAYAR</td>
                    <td><?="Rp " . number_format($data->value_nilai_blm_terbayar,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td>3</td>
                    <td>THR</td>
                    <td><?="Rp " . number_format($data->thr,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td>4</td>
                    <td>FOGGING</td>
                    <td><?="Rp " . number_format($data->fogging,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td>5</td>
                    <td>HBH</td>
                    <td><?="Rp " . number_format($data->hbh,2,',','.');?></td>
                  <tr>
                  <tr>
                    <td colspan='2'>JUMLAH</td>
                    <td><?="Rp " . number_format($hasil=$data->value_nilai + $data->value_nilai_blm_terbayar + $data->thr + $data->fogging + $data->hbh,2,',','.');?></td>
                  <tr>
                </tbody><br><br>
              </table>
                
                <table class="table table-hover" id="table-data" width="100%" cellspacing="0">
                    <tr>
                        <td><font color='white'> TTD </font></td>
                        <td></td>
                        <td><font color='white'> TTD </font></td>
                    </tr>
                    <tr>
                        <td><font color='white'> TTD </font></td>
                        <td></td>
                        <td><font color='white'> TTD </font></td>
                    </tr>
                    <tr>
                        <td><strong></strong></td>
                        <td></td>
                        <td><strong><center>TTD, <?php echo date("d M Y");?></center></strong></td>
                    </tr>
                    <tr>
                        <td><font color='white'> TTD </font></td>
                        <td></td>
                        <td><font color='white'> TTD </font></td>
                    </tr>
                    <tr>
                        <td><strong><center>Bendahara RT</center></strong></td>
                        <td></td>
                        <td><strong><center>Ketua RT</center></strong></td>
                    </tr>
                    <tr>
                        <td><font color='white'> TTD </font></td>
                        <td></td>
                        <td><font color='white'> TTD </font></td>
                    </tr>
                    <tr>
                        <td><font color='white'> TTD </font></td>
                        <td></td>
                        <td><font color='white'> TTD </font></td>
                    </tr>
                    <tr>
                        <td><font color='white'> TTD </font></td>
                        <td></td>
                        <td><font color='white'> TTD </font></td>
                    </tr>
                    <tr>
                        <td><strong><center>BEBEN SUBAGJA</center></strong></td>
                        <td></td>
                        <td><strong><center>IWAN SUPRIYANTO</center></strong></td>
                    </tr>
                </table><br><br>
            <?php endforeach; ?>
    </div>