<!-- Load File jquery.min.js yang ada difolder js -->
<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

<script>
    $(document).ready(function(){
    // Sembunyikan alert validasi kosong
        $("#kosong").hide();
    });
</script>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-print"></i>
        <?=$title?></div>
    <div class="card-body">
              
    <ol class="breadcrumb">
    <a href='<?php echo base_url("Admin_Depkes/Home_Depkes/view_master_depkes"); ?>' class='btn btn-success btn-sm'><i class="fa fa-backward" aria-hidden="true"> Kembali </i></a>
    <font color='#e9ecef'> || </font>
    <a href="<?php echo base_url("assets/Document/format.xlsx"); ?>" class="btn btn-primary btn-sm"><i class="fa fa-download" aria-hidden="true"> Download Format </i></a>

        <div style="clear: both;"></div>
    </ol>

    <!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
    <form method="post" action="<?php echo base_url("Admin_Depkes/Home_Depkes/form"); ?>" enctype="multipart/form-data">
    <!-- Buat sebuah input type file class pull-left berfungsi agar file input berada di sebelah kiri-->
        <input type="file" name="file">
    <!-- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import-->
        <input type="submit" name="preview" value="Lihat">
    </form>

    <?php
    if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
    if(isset($upload_error)){ // Jika proses upload gagal
        echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
        die; // stop skrip
    }

    // Buat sebuah tag form untuk proses import data ke database
    echo "<form method='post' action='".base_url("Admin_Depkes/Home_Depkes/import")."'>";

    // // Buat sebuah div untuk alert validasi kosong
    // echo "<div style='color: red;' id='kosong'>
    //     // Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
    // </div>";

    echo "<br><table border='1' cellpadding='8'>
        <tr>
            <th colspan='11'><center>Preview Data</center></th>
        </tr>
        <tr>
            <th>NAMA</th>
            <th>NO VA</th>
            <th>ALAMAT</th>
            <th>JUMLAH BULAN IPKL TERBAYAR</th>
            <th>NILAI TERBAYAR</th>
            <th>JUMLAH BULAN IPKL BELUM TERBAYAR</th>
            <th>NILAI BELUM TERBAYAR</th>
            <th>TAHUN</th>
            <th>NILAI</th>
            <th>THR</th>
            <th>FOGGING</th>
            <th>HBH</th>
        </tr>";

    $numrow = 1;
    $kosong = 0;

    // Lakukan perulangan dari data yang ada di excel
    // $sheet adalah variabel yang dikirim dari controller
    foreach($sheet as $row){
        // Ambil data pada excel sesuai Kolom
        $nama = $row['A'];
        $no_va = $row['B'];
        $alamat = $row['C'];
        $jml_bulan_ipkl_terbayar = $row['D'];
        $value_nilai = $row['E'];
        $jml_bulan_ipkl_blm_terbayar = $row['F'];
        $value_nilai_blm_terbayar = $row['G'];
        $tahun = $row['H'];
        $nilai_jml_bulan_ipkl = $row['I'];
        $thr = $row['J'];
        $fogging = $row['K'];
        $hbh = $row['L'];

        // Cek jika semua data tidak diisi
        if(empty($nama) && empty($no_va) && empty($alamat))
            continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

        // Cek $numrow apakah lebih dari 1
        // Artinya karena baris pertama adalah nama-nama kolom
        // Jadi dilewat saja, tidak usah diimport
        if($numrow > 1){
            // Validasi apakah semua data telah diisi
            $nama_isi = ( ! empty($nama))? "" : " style='background: #E07171;'";
            $no_va_isi = ( ! empty($no_va))? "" : " style='background: #E07171;'";
            $alamat_isi = ( ! empty($alamat))? "" : " style='background: #E07171;'";
            $jml_bulan_ipkl_terbayar_isi = ( ! empty($jml_bulan_ipkl_terbayar))? "" : " style='background: #E07171;'";
            $value_nilai_isi = ( ! empty($value_nilai))? "" : " style='background: #E07171;'";
            $jml_bulan_ipkl_blm_terbayar_isi = ( ! empty($jml_bulan_ipkl_blm_terbayar))? "" : " style='background: #E07171;'";
            $value_nilai_blm_terbayar_isi = ( ! empty($value_nilai_blm_terbayar))? "" : " style='background: #E07171;'";
            $tahun_isi = ( ! empty($tahun))? "" : " style='background: #E07171;'";
            $nilai_jml_bulan_ipkl_isi = ( ! empty($nilai_jml_bulan_ipkl))? "" : " style='background: #E07171;'";
            $thr_isi = ( ! empty($thr))? "" : " style='background: #E07171;'";
            $fogging_isi = ( ! empty($fogging))? "" : " style='background: #E07171;'";
            $hbh_isi = ( ! empty($hbh))? "" : " style='background: #E07171;'";

            // Jika salah satu data ada yang kosong
            if(empty($nama) or empty($no_va) or empty($alamat)){
                $kosong++; // Tambah 1 variabel $kosong
            }

            echo "<tr>";
                echo "<td".$nama_isi.">".$nama."</td>";
                echo "<td".$no_va_isi.">".$no_va."</td>";
                echo "<td".$alamat_isi.">".$alamat."</td>";
                echo "<td".$jml_bulan_ipkl_terbayar_isi.">".$jml_bulan_ipkl_terbayar."</td>";
                echo "<td".$value_nilai_isi.">".$value_nilai."</td>";
                echo "<td".$jml_bulan_ipkl_blm_terbayar_isi.">".$jml_bulan_ipkl_blm_terbayar."</td>";
                echo "<td".$value_nilai_blm_terbayar_isi.">".$value_nilai_blm_terbayar."</td>";
                echo "<td".$tahun_isi.">".$tahun."</td>";
                echo "<td".$nilai_jml_bulan_ipkl_isi.">".$nilai_jml_bulan_ipkl."</td>";
                echo "<td".$thr_isi.">".$thr."</td>";
                echo "<td".$fogging_isi.">".$fogging."</td>";
                echo "<td".$hbh_isi.">".$hbh."</td>";
            echo "</tr>";
        }
        $numrow++; // Tambah 1 setiap kali looping
    }
    echo "</table>";

    // Cek apakah variabel kosong lebih dari 1
    // Jika lebih dari 1, berarti ada data yang masih kosong
    if($kosong > 1){
        ?> 
        <script>
            $(document).ready(function(){
            // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
            $("#jumlah_kosong").html('<?php echo $kosong; ?>');

            $("#kosong").show(); // Munculkan alert validasi kosong
            });
        </script>
        <?php
    }else{ // Jika semua data sudah diisi
        echo "<hr>";
        // Buat sebuah tombol untuk mengimport data ke database
        echo "<button type='submit' name='import' class='btn btn-primary btn-sm'><i class='fa fa-upload' aria-hidden='true'> Import</i></button>";
        echo "<font color='white'> || </font><a href='".base_url("Admin_Depkes/Home_Depkes/view_master_depkes")."' class='btn btn-primary btn-sm'><i class='fa fa-backward' aria-hidden='true'> Batal</i></a>";
    }
    echo "</form>";
    }
    ?>
        </div>
    </div>