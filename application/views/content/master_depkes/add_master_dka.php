<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        <?=$title?></div>
    <div class="card-body">
        
    <ol class="breadcrumb">
        <a href='<?php echo base_url("Admin_Depkes/Home_Depkes/view_master_depkes"); ?>' class='btn btn-success btn-sm'><i class="fa fa-backward" aria-hidden="true"> Kembali </i></a>
        <div style="clear: both;"></div>
    </ol>

        <div class="col-md-12">
	       <?=form_open_multipart('Admin_Depkes/Home_Depkes/add_master_depkes/',['class'=>'form-horizontal'])?>
	       <?php $error = form_error("nama", "<p class='text-danger'>", '</p>'); ?>
	         <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	         	<label class="col-sm-2 control-label">Nama :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="nama" value="<?= set_value('nama') ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("no_va", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">No VA :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="no_va" value="<?= set_value('no_va') ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>

	          <?php $error = form_error("alamat", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Alamat :</label>
	            <div class="col-sm-10">
                    <textarea type="text" class="form-control" name="alamat" value="<?= set_value('alamat') ?>"></textarea>
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("jml_bulan_ipkl_terbayar", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Jumlah Bulan IPKL Terbayar:</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" onchange="hitung();" id="jml_bulan_ipkl_terbayar" name="jml_bulan_ipkl_terbayar" value="<?= set_value('jml_bulan_ipkl_terbayar') ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>

	          <?php $error = form_error("jml_bulan_ipkl_blm_terbayar", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Jumlah Bulan IPKL Belum Terbayar:</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" onchange="hitung();" id="jml_bulan_ipkl_blm_terbayar" name="jml_bulan_ipkl_blm_terbayar" value="<?= set_value('jml_bulan_ipkl_blm_terbayar') ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("tahun", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Tahun :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="tahun" value="<?= set_value('tahun') ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("nilai_jml_bulan_ipkl", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Nilai IPKL :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" onchange="hitung();" id="nilai_jml_bulan_ipkl" name="nilai_jml_bulan_ipkl" value="<?= set_value('nilai_jml_bulan_ipkl') ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("value_nilai", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Nilai Bulan IPKL Terbayar:</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" id="value_nilai" name="value_nilai" value="<?= set_value('value_nilai') ?>" readonly>
	              <?php echo $error; ?>
	            </div>
	          </div>

	          <?php $error = form_error("value_nilai_blm_terbayar", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">Nilai Bulan IPKL Belum Terbayar:</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" id="value_nilai_blm_terbayar" name="value_nilai_blm_terbayar" value="<?= set_value('value_nilai_blm_terbayar') ?>" readonly>
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("thr", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">THR :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="thr" value="<?= set_value('thr') ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("fogging", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">FOGGING :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="fogging" value="<?= set_value('fogging') ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <?php $error = form_error("hbh", "<p class='text-danger'>", '</p>'); ?>
	          <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
	            <label class="col-sm-2 control-label">HBH :</label>
	            <div class="col-sm-10">
	              <input type="text" class="form-control" name="hbh" value="<?= set_value('hbh') ?>">
	              <?php echo $error; ?>
	            </div>
	          </div>
	          
	          <div class="form-group">
	            <div class="col-sm-offset-2 col-sm-10">
                  <a href='<?php echo base_url("Admin_Depkes/Home_Depkes/view_master_depkes"); ?>' class='btn btn-success'><i class="fa fa-backward" aria-hidden="true"> Kembali </i></a>
	              <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-check"> Simpan</i></button>
	            </div>
	          </div>
	       </form>
	    </div>
        
        </div>
    </div>
    
    <script>
        function hitung() {
            var number1=document.getElementById("jml_bulan_ipkl_terbayar").value;
            var number2=document.getElementById("jml_bulan_ipkl_blm_terbayar").value;
            var number3=document.getElementById("nilai_jml_bulan_ipkl").value;
            hasil1=parseInt(number1)*parseInt(number3);
            hasil2=parseInt(number2)*parseInt(number3);
            document.getElementById("value_nilai").value=hasil1;
            document.getElementById("value_nilai_blm_terbayar").value=hasil2;
        }
    </script>